package ru.grishin.tm.util;

import java.security.MessageDigest;

public final class HashUtil {

    public static String PASSWORD_TO_HASH(final String password) {
        try {
            final byte[] bytesPassword = password.getBytes("UTF-8");
            return new String(MessageDigest.getInstance("MD5").digest(bytesPassword));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}