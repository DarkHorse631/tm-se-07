package ru.grishin.tm;

import ru.grishin.tm.bootstrap.Bootstrap;
import ru.grishin.tm.command.project.*;
import ru.grishin.tm.command.system.AboutCommand;
import ru.grishin.tm.command.system.HelpCommand;
import ru.grishin.tm.command.system.LoginCommand;
import ru.grishin.tm.command.system.LogoutCommand;
import ru.grishin.tm.command.task.*;
import ru.grishin.tm.command.user.*;

public final class Application {

    public static final Class[] CLASSES = {
            HelpCommand.class,
            ProjectCreateCommand.class, ProjectRemoveCommand.class,
            ProjectShowAllCommand.class, ProjectClearCommand.class,
            ProjectUpdateCommand.class, ProjectMergeCommand.class,
            TaskCreateCommand.class, TaskRemoveCommand.class,
            TaskUpdateCommand.class, TaskShowAllCommand.class,
            TaskMergeCommand.class, UserCreateCommand.class,
            LoginCommand.class, LogoutCommand.class,
            UserUpdateCommand.class, UserRemoveCommand.class,
            UserUpdatePasswordCommand.class, UserShowAllCommand.class,
            AboutCommand.class
    };

    public static void main(String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
        bootstrap.start();
    }

}