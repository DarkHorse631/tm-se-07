package ru.grishin.tm.service;

import ru.grishin.tm.api.ProjectRepository;
import ru.grishin.tm.api.ProjectService;
import ru.grishin.tm.entity.Project;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public final class ProjectServiceImpl extends AbstractService<Project> implements ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name, final String description, final Date dateStart, final Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.persist(new Project(UUID.randomUUID().toString(), userId, name, description, dateStart, dateFinish));
    }

    @Override
    public void remove(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        projectRepository.remove(userId, id);
    }

    @Override
    public void update(final String userId, final String id, final String name, final String description, final Date dateStart, final Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (projectRepository.isExist(id))
            projectRepository.update(userId, id, name, description, dateStart, dateFinish);
    }

    @Override
    public Project findOne(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOne(userId, id);
    }

    @Override
    public void merge(final Project project) {
        if (project.getUserId() == null || project.getUserId().isEmpty()) return;
        if (project.getId() == null || project.getId().isEmpty()) return;
        projectRepository.merge(project);
    }

    @Override
    public Collection<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

}
