package ru.grishin.tm.service;

import ru.grishin.tm.api.UserRepository;
import ru.grishin.tm.api.UserService;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.util.HashUtil;

import java.util.Collection;
import java.util.UUID;

public final class UserServiceImpl extends AbstractService<User> implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User login(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final User user = userRepository.findLogin(login);
        if (user.getPassword().equals(HashUtil.PASSWORD_TO_HASH(password))) return user;
        return null;
    }

    @Override
    public void registryUser(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findLogin(login) != null) return;
        userRepository.persist(new User(UUID.randomUUID().toString(), login, HashUtil.PASSWORD_TO_HASH(password), RoleType.USER));
    }

    @Override
    public void registryAdmin(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        userRepository.persist(new User(UUID.randomUUID().toString(), login, HashUtil.PASSWORD_TO_HASH(password), RoleType.ADMIN));
    }

    @Override
    public void update(final String id, final String login, final String password, final RoleType roleType) {
        if (id == null || id.isEmpty()) return;
        if (userRepository.findOne(id) == null) return;
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.isExist(id))
            userRepository.update(id, login, password, roleType);
    }

    @Override
    public void remove(final String id) {
        if (id == null || id.isEmpty()) return;
        if (userRepository.findOne(id) == null) return;
        userRepository.remove(id);
    }

    @Override
    public User findOne(final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOne(id);
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void merge(final User user) {
        if (user.getId() == null || user.getId().isEmpty()) return;
        userRepository.merge(user);
    }

    @Override
    public void updatePassword(final String id, final String currentPassword, final String newPassword) {
        if (id == null || id.isEmpty()) return;
        if (currentPassword == null || currentPassword.isEmpty()) return;
        if (newPassword == null || newPassword.isEmpty()) return;
        if (userRepository.findOne(id) == null) return;
        if (HashUtil.PASSWORD_TO_HASH(currentPassword).equals(userRepository.findOne(id).getPassword()))
            userRepository.updatePassword(id, HashUtil.PASSWORD_TO_HASH(newPassword));
    }

    @Override
    protected User findOne(final String userId, final String id) {
        return null;
    }

    @Override
    protected Collection<User> findAll(final String userId) {
        return null;
    }
}
