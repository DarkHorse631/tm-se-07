package ru.grishin.tm.service;

import java.util.Collection;

public abstract class AbstractService<T> {

    abstract T findOne(String userId, String id);

    abstract Collection<T> findAll(String userId);
}
