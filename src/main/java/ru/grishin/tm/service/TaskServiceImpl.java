package ru.grishin.tm.service;

import ru.grishin.tm.api.TaskRepository;
import ru.grishin.tm.api.TaskService;
import ru.grishin.tm.entity.Task;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public final class TaskServiceImpl extends AbstractService<Task> implements TaskService {
    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String projectId, final String userId, final String name, final String description, final Date dateStart, final Date dateFinish) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        taskRepository.persist(new Task(projectId, userId, UUID.randomUUID().toString(), name, description, dateStart, dateFinish));
    }

    @Override
    public void remove(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return;

        if (id == null || id.isEmpty()) return;
        taskRepository.remove(userId, id);
    }

    @Override
    public void update(final String userId, final String id, final String name, final String description, final Date dateStart, final Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (taskRepository.isExist(id))
            taskRepository.update(userId, id, name, description, dateStart, dateFinish);
    }

    @Override
    public void deleteByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.deleteByProjectId(userId, projectId);
    }


    @Override
    public Task findOne(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(userId, id);
    }

    @Override
    public Collection<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAll(userId);
    }

    @Override
    public void merge(final Task task) {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) return;
        if (task.getUserId() == null || task.getUserId().isEmpty()) return;
        if (task.getId() == null || task.getId().isEmpty()) return;
        taskRepository.merge(task);
    }

}
