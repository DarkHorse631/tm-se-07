package ru.grishin.tm.api;

import ru.grishin.tm.entity.Task;

import java.util.Collection;
import java.util.Date;

public interface TaskService {

    void create(String projectId, String userId, String name, String description, Date startDate, Date completionDate);

    Collection<Task> findAll(String userId);

    Task findOne(String userId, String id);

    void remove(String userId, String id);


    void update(String userId, String id, String name, String description, Date startDate, Date completionDate);

    void merge(Task task);

    void deleteByProjectId(String userId, String projectId);
}
