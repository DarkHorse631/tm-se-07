package ru.grishin.tm.api;

import ru.grishin.tm.entity.Task;

import java.util.Collection;
import java.util.Date;

public interface TaskRepository {

    Collection<Task> findAll(String userId);

    void persist(Task task);

    void merge(Task task);

    void insert(String projectId, String userId, String id, String name, String description, Date dateStart, Date dateFinish);

    void remove(String userId, String id);

    Task findOne(String userId, String id);

    void deleteByProjectId(String userId, String projectId);

    void mergeAll(Task... tasks);

    void update(String userId, String id, String name, String description, Date dateStart, Date dateFinish);

    void removeAll();

    boolean isExist(String id);
}
