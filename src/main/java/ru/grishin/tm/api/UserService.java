package ru.grishin.tm.api;

import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Collection;

public interface UserService {

    Collection<User> findAll();

    User findOne(String Id);

    User login(String login, String password);

    void registryUser(String login, String password);

    void registryAdmin(String login, String password);

    void update(String id, String login, String password, RoleType roleType);

    void remove(String id);

    void merge(User user);

    void updatePassword(String id, String currentPassword, String newPassword);
}
