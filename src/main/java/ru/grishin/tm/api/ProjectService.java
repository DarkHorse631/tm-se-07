package ru.grishin.tm.api;

import ru.grishin.tm.entity.Project;

import java.util.Collection;
import java.util.Date;

public interface ProjectService {

    Collection<Project> findAll(String userId);

    Project findOne(String userId, String id);

    void create(String userId, String name, String description, Date startDate, Date completionDate);

    void remove(String userId, String id);

    void update(String userId, String id, String name, String description, Date startDate, Date completionDate);

    void merge(Project project);
}
