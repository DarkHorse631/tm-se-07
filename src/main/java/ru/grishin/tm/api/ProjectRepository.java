package ru.grishin.tm.api;

import ru.grishin.tm.entity.Project;

import java.util.Collection;
import java.util.Date;

public interface ProjectRepository {

    Collection<Project> findAll(String userId);

    void persist(Project project);

    void merge(Project project);

    void insert(String id, String userId, String name, String description, Date startDate, Date completionDate);

    void mergeAll(Project... projects);

    Project findOne(String userId, String id);

    void remove(String userId, String id);

    void update(String userId, String id, String name, String description, Date startDate, Date completionDate);

    void clearProjectList();

    boolean isExist(String id);
}
