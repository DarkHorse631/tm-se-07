package ru.grishin.tm.api;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.User;


import java.util.Collection;
import java.util.Scanner;

public interface ServiceLocator {
    void init(Class... classes) throws Exception;

    void registry(Class clazz) throws Exception;

    void start() throws Exception;

    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    Scanner getScanner();

    void setCurrentUser(User currentUser);

    User getCurrentUser();

    Collection<AbstractCommand> getCommands();
}
