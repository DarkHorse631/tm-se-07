package ru.grishin.tm.api;

import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Collection;

public interface UserRepository {

    Collection<User> findAll();

    void persist(User user);

    void merge(User user);

    void insert(String id, String login, String password, RoleType roleType);

    void update(String id, String login, String password, RoleType roleType);

    void remove(String id);

    void updatePassword(String id, String password);

    User findOne(String id);

    User findLogin(String login);

    boolean isExist(String id);
}
