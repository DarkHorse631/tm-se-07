package ru.grishin.tm.command;

import ru.grishin.tm.api.ServiceLocator;
import ru.grishin.tm.enumerate.RoleType;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract RoleType [] roles();

}
