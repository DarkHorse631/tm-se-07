package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public final class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "tu";
    }

    @Override
    public String getDescription() {
        return "Update task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Update task--");
        System.out.print("Enter task id:");
        final String taskId = serviceLocator.getScanner().nextLine();
        System.out.print("Enter new name:");
        final String name = serviceLocator.getScanner().nextLine();
        System.out.print("Enter new description:");
        final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskService().update(serviceLocator.getCurrentUser().getId(), taskId, name, description, new Date(), new Date());
        System.out.println("[TASK UPDATED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
