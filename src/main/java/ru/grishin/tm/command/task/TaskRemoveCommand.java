package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "td";
    }

    @Override
    public String getDescription() {
        return "Remove task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Remove task from project--");
        System.out.print("Enter task id: ");
        final String taskId = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskService().remove(serviceLocator.getCurrentUser().getId(),taskId);
        System.out.println("[TASK DELETED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER,RoleType.ADMIN};
    }
}
