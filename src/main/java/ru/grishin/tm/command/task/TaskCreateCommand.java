package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "tc";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Create task into project--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.print("Enter task name: ");
        final String name = serviceLocator.getScanner().nextLine();
        System.out.print("Enter task description: ");
        final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskService().create(projectId, serviceLocator.getCurrentUser().getId(), name, description, new Date(), new Date());
        System.out.println("[TASK CREATED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
