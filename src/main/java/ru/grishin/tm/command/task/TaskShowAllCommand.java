package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class TaskShowAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "tsa";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Show all tasks--");
        for(Object o : serviceLocator.getTaskService().findAll(serviceLocator.getCurrentUser().getId()))
            System.out.println(o);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER,RoleType.ADMIN};
    }
}
