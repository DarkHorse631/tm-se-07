package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public final class TaskMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "tm";
    }

    @Override
    public String getDescription() {
        return "Merge task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Merge task--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.print("Enter task id: ");
        final String id = serviceLocator.getScanner().nextLine();
        System.out.print("Enter task name: ");
        final String name = serviceLocator.getScanner().nextLine();
        System.out.print("Enter task description: ");
        final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskService().merge(new Task(projectId, serviceLocator.getCurrentUser().getId(), id, name, description, new Date(), new Date()));
        System.out.println("[TASK MERGED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
