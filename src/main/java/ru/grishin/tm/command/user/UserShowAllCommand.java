package ru.grishin.tm.command.user;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class UserShowAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "usa";
    }

    @Override
    public String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Show all users--");
        for(Object o : serviceLocator.getUserService().findAll())
            System.out.println(o);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN};
    }
}
