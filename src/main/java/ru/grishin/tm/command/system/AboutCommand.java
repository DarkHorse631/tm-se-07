package ru.grishin.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.apache.log4j.BasicConfigurator;
import ru.grishin.tm.command.AbstractCommand;

import ru.grishin.tm.enumerate.RoleType;

public final class AboutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Information about program.";
    }

    @Override
    public void execute() throws Exception {
        BasicConfigurator.configure();
        System.out.println(
                " Developer: " + Manifests.read("Developer") +
                        "\n Version: " + Manifests.read("Version") +
                        "\n BuildNumber (" + Manifests.read("BuildNumber") + ")");

    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
