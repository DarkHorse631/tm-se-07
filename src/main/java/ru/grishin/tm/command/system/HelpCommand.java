package ru.grishin.tm.command.system;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all CLASSES";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommands()) {
            System.out.println("[" + command.getName() + "] : " + command.getDescription());
        }
        System.out.println("[exit] : Exit form application.");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
