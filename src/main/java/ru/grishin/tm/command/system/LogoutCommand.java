package ru.grishin.tm.command.system;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class LogoutCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "Out from application.";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.setCurrentUser(null);
        System.out.println("[LOGOUT COMPLETE]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
