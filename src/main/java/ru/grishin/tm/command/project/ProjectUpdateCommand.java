package ru.grishin.tm.command.project;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public final class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "pu";
    }

    @Override
    public String getDescription() {
        return "Update project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Update project--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.print("Enter new name: ");
        final String name = serviceLocator.getScanner().nextLine();
        System.out.print("Enter new description: ");
        final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().update(serviceLocator.getCurrentUser().getId(), projectId, name, description, new Date(), new Date());
        System.out.println("[PROJECT UPDATED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
