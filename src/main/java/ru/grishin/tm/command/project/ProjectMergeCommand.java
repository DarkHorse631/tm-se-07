package ru.grishin.tm.command.project;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public final class ProjectMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "pm";
    }

    @Override
    public String getDescription() {
        return "Merge project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Merge project--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.print("Enter project name: ");
        final String name = serviceLocator.getScanner().nextLine();
        System.out.print("Enter project description: ");
        final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().merge(new Project(serviceLocator.getCurrentUser().getId(), projectId, name, description, new Date(), new Date()));
        System.out.println("[PROJECT MERGED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
