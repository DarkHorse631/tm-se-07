package ru.grishin.tm.command.project;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "pd";
    }

    @Override
    public String getDescription() {
        return "Remove project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Remove project--");
        System.out.print("Enter the project id: ");
        final String projectId = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().remove(serviceLocator.getCurrentUser().getId(), projectId);
        serviceLocator.getTaskService().deleteByProjectId(serviceLocator.getCurrentUser().getId(), projectId);
        System.out.println("[PROJECT DELETED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
