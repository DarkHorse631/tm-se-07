package ru.grishin.tm.command.project;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "pc";
    }

    @Override
    public String getDescription() {
        return "Create project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Create project--");
        System.out.print("Enter name: ");
        final String name = serviceLocator.getScanner().nextLine();
        System.out.print("Enter description: ");
        final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().create(serviceLocator.getCurrentUser().getId(), name, description, new Date(), new Date());
        System.out.println("[PROJECT CREATED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
