package ru.grishin.tm.entity;

import ru.grishin.tm.enumerate.RoleType;

public final class User {

    private String id;
    private String login;
    private String password;
    private RoleType roleType;


    public User(String id, String login, String password, RoleType roleType) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.roleType = roleType;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return "User id:[" + id +
                "] user login: [" + login +
                "] password: " + password +
                "] roleType: " + roleType + "]";
    }
}
