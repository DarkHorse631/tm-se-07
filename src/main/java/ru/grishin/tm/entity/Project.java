package ru.grishin.tm.entity;

import ru.grishin.tm.util.DateUtil;

import java.util.Date;

public final class Project {

    private String userId;
    private String id;
    private String name;
    private String description;
    private Date dateStart;
    private Date dateFinish;

    public Project() {
    }

    public Project(String id, String userId, String name, String description, Date dateStart, Date dateFinish) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
    }

    public String getUserId() {
        return userId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "Project ID = [" + id +
                "], User ID = [" + userId +
                "], Project name = [" + name +
                "], Description =[" + description +
                "], Start date = [" + DateUtil.FORMAT_DATE(dateStart) +
                "], Completion date =[" + DateUtil.FORMAT_DATE(dateFinish) + "]";
    }

}