package ru.grishin.tm.repository;

import ru.grishin.tm.api.ProjectRepository;
import ru.grishin.tm.entity.Project;

import java.util.*;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements ProjectRepository {

    private final Map<String, Project> projects = new LinkedHashMap<>();

    @Override
    public void persist(final Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void insert(final String id, final String userId, final String name, final String description, final Date startDate, final Date completionDate) {
        projects.put(id, new Project(id, userId, name, description, startDate, completionDate));
    }

    @Override
    public void merge(final Project project) {
        if (projects.containsKey(project.getId()))
            update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
        else
            insert(project.getId(), project.getUserId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
    }

    @Override
    public void mergeAll(final Project... projects) {
        for (Project project : projects)
            merge(project);
    }

    public Project findOne(final String userId, final String id) {
        final Project project = projects.get(id);
        if (project == null) return null;
        if (project.getUserId().equals(userId)) return project;
        return null;
    }

    public void remove(final String userId, final String id) {
        final Project project = projects.get(id);
        if (project == null) return;
        if (project.getUserId().equals(userId)) projects.remove(id);
    }

    @Override
    public Collection<Project> findAll(final String userId) {
        final Collection<Project> result = new LinkedList<>();
        Iterator<Map.Entry<String, Project>> entryIterator = projects.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        return result;
    }

    @Override
    public void update(final String userId, final String id, final String name, final String description, final Date startDate, final Date completionDate) {
        final Project project = projects.get(id);
        if (project.getUserId().equals(userId)) return;
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(startDate);
        project.setDateFinish(completionDate);
        projects.put(id, project);
    }

    @Override
    public void clearProjectList() {
        projects.clear();
    }

    public boolean isExist(final String id) {
        return projects.containsKey(id);
    }
}
