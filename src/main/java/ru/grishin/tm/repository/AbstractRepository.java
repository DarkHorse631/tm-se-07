package ru.grishin.tm.repository;

import java.util.Collection;

public abstract class AbstractRepository<T> {

    public abstract Collection<T> findAll(String userId);

    public abstract void persist(T t);

    public abstract void merge(T t);
}
