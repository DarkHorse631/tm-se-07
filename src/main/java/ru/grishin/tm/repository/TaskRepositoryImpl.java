package ru.grishin.tm.repository;

import ru.grishin.tm.api.TaskRepository;
import ru.grishin.tm.entity.Task;

import java.util.*;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    @Override
    public void persist(final Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void insert(final String projectId, final String userId, final String id, final String name, final String description, final Date dateStart, final Date dateFinish) {
        tasks.put(id, new Task(projectId, userId, id, name, description, dateStart, dateFinish));
    }

    public void remove(final String userId, final String id) {
        final Task task = tasks.get(id);
        if (task == null) return;
        if (task.getUserId().equals(userId)) tasks.remove(id);
    }

    public Task findOne(final String userId, final String id) {
        final Task task = tasks.get(id);
        if (task == null) return null;
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    public void deleteByProjectId(final String userId, final String projectId) {
        final Iterator<Map.Entry<String, Task>> entryIterator = tasks.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getProjectId().equals(projectId)) entryIterator.remove();
        }
    }

    @Override
    public Collection<Task> findAll(final String userId) {
        final Collection<Task> result = new LinkedList<>();
        Iterator<Map.Entry<String, Task>> entryIterator = tasks.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        return result;
    }

    @Override
    public void merge(final Task task) {
        if (tasks.containsKey(task.getId()))
            update(task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
        else
            insert(task.getProjectId(), task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
    }

    @Override
    public void mergeAll(final Task... tasks) {
        for (Task task : tasks) {
            merge(task);
        }
    }

    @Override
    public void update(final String userId, final String id, final String name, final String description, final Date dateStart, final Date dateFinish) {
        final Task task = tasks.get(id);
        if (!task.getUserId().equals(userId)) return;
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        tasks.put(task.getId(), task);
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

    public boolean isExist(final String id) {
        return tasks.containsKey(id);
    }
}
