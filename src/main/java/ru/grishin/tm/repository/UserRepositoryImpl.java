package ru.grishin.tm.repository;

import ru.grishin.tm.api.UserRepository;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.entity.User;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

public final class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {

    private final Map<String, User> users = new LinkedHashMap<>();

    @Override
    public Collection<User> findAll(String userId) {
        return null;
    }

    @Override
    public void persist(final User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void insert(final String id, final String login, final String password, final RoleType roleType) {
        users.put(id, new User(id, login, password, roleType));
    }

    @Override
    public void update(final String id, final String login, final String password, final RoleType roleType) {
        final User user = users.get(id);
        user.setLogin(login);
        user.setPassword(password);
        user.setRoleType(roleType);
        users.put(user.getId(), user);
    }

    @Override
    public void remove(final String id) {
        users.remove(id);
    }

    @Override
    public void merge(final User user) {
        if (users.containsKey(user.getId()))
            update(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
        else
            insert(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
    }

    @Override
    public void updatePassword(final String id, final String password) {
        users.get(id).setPassword(password);
    }

    @Override
    public User findOne(final String id) {
        return users.get(id);
    }

    @Override
    public User findLogin(final String login) {
        for (User user : users.values()) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    @Override
    public Collection<User> findAll() {
        Collection<User> output = new LinkedList<>();
        return output = users.values();
    }

    public boolean isExist(String id) {
        return users.containsKey(id);
    }

}
